package spite

import (
	"bytes"
	"encoding/json"
	"encoding/xml"
	"fmt"
	"html/template"
	"log"
	"net/http"

	"github.com/BurntSushi/toml"
	"github.com/ghodss/yaml"
)

const (
	HeaderContentType = "Content Type"
)

// OpenTemplate will construct a template using the base file
func OpenTemplate(base string) (*template.Template, error) {
	var file1path = fmt.Sprintf("%s/%s.%s",
		Setup.Get("template_dir"),
		base,
		Setup.Get("template_ext"))

	var tmp, err = template.ParseFiles(file1path)

	if err != nil {
		log.Println(err)
		return nil, err
	}

	return tmp, nil
}

// OpenTemplates will make a new template user base and inner to construct a ladderd template.
func OpenTemplates(base, inner string) (*template.Template, error) {
	var file1path = fmt.Sprintf("%s/%s.%s",
		Setup.Get("template_dir"),
		base,
		Setup.Get("template_ext"))

	var file2path = fmt.Sprintf("%s/%s.%s",
		Setup.Get("template_dir"),
		inner,
		Setup.Get("template_ext"))

	var tmp, err = template.ParseFiles(file1path, file2path)

	if err != nil {
		log.Println(err)
		return nil, err
	}

	return tmp, nil
}

// ContentTypeHTML will set the content type of the response to HTML.
func ContentTypeHTML(w http.ResponseWriter) {
	w.Header().Add(HeaderContentType, "text/html")
}

// WriteHeader will write a key value pair the header of the http response.
func WriteHeader(w http.ResponseWriter, key string, value string) {
	w.Header().Add(key, value)
}

// Start will start a web server.
func Start() {
	if Setup.Get("tls") == "true" {
		http.ListenAndServeTLS(":"+Setup.Get("port"), Setup.Get("tls_cert"), Setup.Get("tls_key"), nil)
	} else {
		http.ListenAndServe(":"+Setup.Get("port"), nil)
	}
}

func WriteApi(w http.ResponseWriter, model interface{}) {
	if Setup.Get("api_encoding") == "xml" {
		w.Header().Add("Content Type", "application/xml")

		xmldata, err := xml.Marshal(model)

		if err != nil {
			w.Write([]byte(""))
			CheckError(w, err)
		}

		w.Write([]byte(xml.Header))
		w.Write([]byte(xmldata))
	} else if Setup.Get("api_encoding") == "yaml" {
		w.Header().Add("Content Type", "application/yaml")

		yamldata, err := yaml.Marshal(model)

		if err != nil {
			w.Write([]byte(""))
			CheckError(w, err)
		}

		w.Write([]byte(yamldata))
	} else if Setup.Get("api_encoding") == "toml" {
		var data bytes.Buffer

		w.Header().Add("Content Type", "application/toml")

		var err = toml.NewEncoder(&data).Encode(model)

		if err != nil {
			w.Write([]byte(""))
			CheckError(w, err)
		}

		w.Write([]byte(data.Bytes()))
	} else {
		w.Header().Add("Content Type", "application/json")

		js, err := json.Marshal(model)

		if err != nil {
			w.Write([]byte("{}"))
			CheckError(w, err)
		}

		w.Write([]byte(js))
	}
}

func CheckError(w http.ResponseWriter, err error) {
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func CheckErrorWithCode(w http.ResponseWriter, err error, code int) {
	if err != nil {
		http.Error(w, err.Error(), code)
	}
}
