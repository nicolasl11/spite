package spite

import (
	"encoding/json"
	"io/ioutil"
	"log"
)

// basic config file
// {
//    "port": "8080",
//    "db_connection": "database/database.db",
//    "db_driver": "sqlite3",
//    "template_dir": "templates",
//    "template_ext": "gohtml",
//    "api_encoding": "json",
//    "tls": "false",
//    "tls_key": "key.pem",
//    "tls_cert": "cert.pem"
// }

// ConfigFileDefault is the default config file path.
const ConfigFileDefault = "config/settings.json"

// ConfigFile is the path string of the config file.
var ConfigFile = ConfigFileDefault

// Setup stores the config settings from the config file.
var Setup = DefaultSetup{}

// interface that the user need to create a setup object to place his/her setup
// into.
type Getter interface {
	Get(string) string
}

type DefaultSetup struct {
	settingsMap map[string]string
}

func (this DefaultSetup) Get(key string) string {

	if this.settingsMap == nil {
		file, err := ioutil.ReadFile(ConfigFile)

		if err != nil {
			log.Println(err.Error())
			return ""
		}

		err = json.Unmarshal(file, &this.settingsMap)

		if err != nil {
			log.Println(err.Error())
			return ""
		}
	}

	if val, ok := this.settingsMap[key]; ok {
		return val
	}

	log.Println("key not found in settings.json:", key)
	return ""
}
