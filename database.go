package spite

import (
	"log"

	"github.com/jinzhu/gorm"
	_ "github.com/mattn/go-sqlite3"
)

// Database holds the lower level object references to gorm database.
type Database struct {
	db *gorm.DB
}

func (this *Database) init() error {
	if this.db != nil {
		log.Println("[db init] the database is already initialized")
	}

	driver := Setup.Get("db_driver")
	connection := Setup.Get("db_connection")

	db, err := gorm.Open(driver, connection)

	if err != nil {
		return err
	}

	this.db = db

	return nil
}

// AddModel adds the appropiate table to the database for model.
func (this *Database) AddModel(model interface{}) {
	if this.db == nil {
		this.init()
	}

	this.db.CreateTable(model)
	this.db.AutoMigrate(model)
}

// Close will close the connection to the database.
func (this *Database) Close() {
	if this.db == nil {
		return
	}

	this.db.Close()
}

// Execute allows access to the database.
func (this *Database) Execute() *gorm.DB {
	if this.db == nil {
		this.init()
	}

	return this.db
}
